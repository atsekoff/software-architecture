**Software Architecture**

*Create a virtual shop - practice the use of UML, apply design patterns to improve project's structure, use unit tests to ensure proper performance.*

Consists of four projects:
 1. Shop_API - a .NET library project representing the Model and Controller for the shop.
 2. Shop_GXPE - a GXPEngine View project. Uses the Shop_API.dll.
 3. Shop_Unity - a UnityEngine View project. Uses the Shop_API.dll.
 4. Shop_Tests - an NUnit UnitTest project. Tests the functionality of the Shop_API.dll.