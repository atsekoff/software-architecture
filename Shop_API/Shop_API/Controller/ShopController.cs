﻿namespace ShopAPI.Controller
{
  using ShopAPI.Model;

  /// <summary>
  /// This class provides a controller for a Shop. The Controller acts as a public interface for a Shop.
  /// These methods are being called by a View, as it implements the user interface.
  /// </summary>
  public class ShopController
  {
    private readonly ShopModel _shop;

    public ShopController(ShopModel aShop)
    {
      _shop = aShop;
      Browse();
    }

    public void SelectItem(Item aItem)
    {
      if (aItem != null)
        _shop.SelectItem(aItem);
    }

    public void Browse()
    {
      _shop.SelectItemByIndex(0);
    }

    public void Buy()
    {
      _shop.Buy();
    }

    public void Sell()
    {
      _shop.Sell();
    }
  }
}
