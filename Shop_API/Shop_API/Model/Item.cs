﻿namespace ShopAPI.Model
{
  public class Item
  {
    public string Name { get; }
    public string IconName { get; }
    public int Cost { get; }

    internal Item(string aName, string aIconName, int aCost)
    {
      Name = aName;
      IconName = aIconName;
      Cost = aCost;
    }
  }
}
