﻿/* This project builds a .dll of the Shop's Model and Controller.
 * The .dll(Release) is copied to the (UnityDir)/Assets/Libs and (GXPEngine)/Libs folder via a Post-Build Event
 * see Project->Properties->Build Event
 */

using System.Collections.Generic;

namespace ShopAPI.Model
{
  /// <summary>
  /// This class holds the model of the Shop.
  /// </summary>
  public class ShopModel
  {
    const int MAX_MESSAGE_QUEUE_COUNT = 4;

    private int _selectedItemIndex;
    private List<string> _messages;
    private List<Item> _items;

    public ShopModel()
    {
      _selectedItemIndex = 0;
      _messages = new List<string>();
      _items = new List<Item>();

      PopulateInventory(16);
    }

    /// <summary>
    /// Populates the Shop inventory with dummy items
    /// </summary>
    /// <param name="aItemCount"></param>
    private void PopulateInventory(int aItemCount)
    {
      for (int i = 0; i < aItemCount; i++)
      {
        _items.Add(new Item("item" + (i + 1), "item", 10));
      }
    }

    /// <summary>
    /// Adds message to the cache, cleaning when the limit is exceeded
    /// </summary>
    /// <param name="aMessage"></param>
    private void AddMessage(string aMessage)
    {
      _messages.Add(aMessage);
      while (_messages.Count > MAX_MESSAGE_QUEUE_COUNT)
        _messages.RemoveAt(0);
    }

    public void SelectItem(Item aItem)
    {
      if (aItem != null)
        SelectItemByIndex(_items.IndexOf(aItem));
    }

    public void SelectItemByIndex(int aIndex)
    {
      if (aIndex >= 0 && aIndex < _items.Count)
        _selectedItemIndex = aIndex;
    }

    public Item GetSelectedItem()
    {
      return _selectedItemIndex >= 0 && _selectedItemIndex < _items.Count ? _items[_selectedItemIndex] : null;
    }

    public int GetSelectedItemIndex()
    {
      return _selectedItemIndex;
    }

    /// <summary>
    /// returns a copy of the list, so the original is kept intact, 
    /// however this is shallow copy of the original list, so changes in 
    /// the original list will likely influence the copy, apply 
    /// creational patterns like prototype to fix this. 
    /// </summary>
    public List<Item> GetItems()
    {
      return new List<Item>(_items);
    }

    public Item GetItemByIndex(int aIndex)
    {
      return aIndex >= 0 && aIndex < _items.Count ? _items[aIndex] : null;
    }

    public int GetItemCount()
    {
      return _items.Count;
    }

    public string[] GetMessages()
    {
      return _messages.ToArray();
    }

    public void Buy()
    {
      AddMessage("You can't buy this yet!");
    }

    public void Sell()
    {
      AddMessage("You can't sell this yet!");
    }
  }
}
