﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GXPEngine
{
  public static class Const
  {
    public const string DIR_LIB = "../../Libs/";
    public const string DIR_MEDIA = "../../Assets/";
  }
}
