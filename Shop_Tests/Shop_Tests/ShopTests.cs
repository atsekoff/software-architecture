﻿/* 
 * Unit tests for the Shop_API.dll
 */
using NUnit.Framework;
using ShopAPI.Model;

namespace Shop_Tests
{
  [TestFixture]
  public class ShopTests 
  {
    [Test]
    public void Test1()
    {
      var shop = new ShopModel();
      Assert.That(shop.GetItemCount() == 16);
    }
  }
}
